#!/usr/bin/env bash

GO_PATH="/go"
#build_image="docker-golang-build:5e197fe"
build_image="registry.gitlab.com/wjm.elbers/golang-build:2.0.0-debian"

#docker save --output=/builds/CLARIN-ERIC/docker-tester/output/docker-tester:afbd141ecdf8158b4010e5e0f62155d1ce4a8a0e.tar.gz registry.gitlab.com/clarin-eric/docker-tester:afbd141ecdf8158b4010e5e0f62155d1ce4a8a0e

#docker load --input=/builds/CLARIN-ERIC/docker-tester/output/docker-tester:afbd141ecdf8158b4010e5e0f62155d1ce4a8a0e.tar.gz
 #**** Testing image ****
 #Loaded image: registry.gitlab.com/clarin-eric/docker-tester:afbd141ecdf8158b4010e5e0f62155d1ce4a8a0e

build() {
  project_path=${1}
  binary="${2}"

  echo "  Building ${binary}"
  docker run --rm \
    -e "GOPATH=${GO_PATH}" \
    -e "BINARY=${binary}" \
    -v "$PWD/golang/":/go/ \
    -v "$PWD/output":/output \
    -w /go/src/${binary} \
    ${build_image} sh -c 'make'
}

build_all_binaries() {
  #Sourced via build script, so no arrays supported
  set -e
  echo "Building binaries:"
  cd ..
  build "checker" "checker"
  cd ./image
  cp ../output/checker* .
  set +e
}

init_data() {
    LOCAL=0
    if [ "$1" == "local" ]; then
        LOCAL=1
    fi

    #if [ "${LOCAL}" -eq 1 ]; then
        build_all_binaries
    #fi
}

cleanup_data() {
    rm checker* > /dev/null 2>&1 || true
}
