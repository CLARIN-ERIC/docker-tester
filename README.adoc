= Docker test image (`docker-tester`)
:caution-caption: ☡ CAUTION
:important-caption: ❗ IMPORTANT
:note-caption: 🛈 NOTE
:sectanchors:
:sectlinks:
:sectnumlevels: 6
:sectnums:
:source-highlighter: pygments
:tip-caption: 💡 TIP
:toc-placement: preamble
:toc:
:warning-caption: ⚠ WARNING

This image provides a set of basic tests which can be used to validate basic functionality of various services.

== Tester image

=== Compose project integration testing

Example compose project to explain testing a http endpoint (`nginx`) and wait for some other long running process to
finish (i.e. `cypress` for e2e testing):

```
version: '3.1'
services:
  nginx:
    image: "registry.gitlab.com/clarin-eric/docker-alpine-nginx:2.5.0"
    command: --test
    volumes:
      - "test:/test:rw"
      - "./default.conf:/nginx_conf.d/default.conf:ro"
    restart: "no"
    logging:
      driver: json-file

  #Write a *.pid file to /test, then sleep for a while before removing it
  pid-writer:
    image: alpine:latest
    command: sh -c 'echo "Writing /test/pid-writer.pid" && touch /test/pid-writer.pid && sleep 30 && echo "Removing /test/pid-writer.pid" && rm /test/pid-writer.pid'
    volumes:
      - "test:/test:rw"
    logging:
      driver: json-file

  #Run a simple http test on the nginx endpoint and wait for the extra *.pid file to be removed before exiting
  tester:
    image: "registry.gitlab.com/clarin-eric/docker-tester:2.0.0"
    command: http multi --verbose --timeout 60 --http-insecure
    volumes:
      - "./checker.conf:/etc/checker.conf"
      - "test:/test"
    restart: "no"
    logging:
      driver: json-file

volumes:
  test:
    external: false
```

Example `checker.conf` defining multiple HTTP tests:
```
https://nginx/;200
https://nginx/doesnotexist/;404
```

== Checker binary

The checker binary, entrypoint of this image, can be used to run tests against a target. Currently http(s) based testing
and custom testing via specific shell commands is supported.

Global flags available for all commands. Default should be fine in most cases, most common is to increase the total test timeout with the `--timeout` flag:
```
Global Flags:
    --sleep int             Wait this amount of seconds before checking for extensions again (default 1)
    --success-file string   Path to file to signal other container that test succeeded (default "/test/done")
    --timeout int           Total test timeout in seconds (default 60)
-v, --verbose               Run in verbose mode
    --wait-for-ext string   Wait until all files with this extension are removed (default ".pid")
```

Run a single http check:
```
checker http single [flags]

Flags:
  -k, --http-insecure         Allow connections to SSL sites without certs or with invalid certs
  -t, --http-timeout int      Timeout in seconds (default 10)
  -e, --http-expected int     Expected HTTP response code (default 200)
  -u, --http-url string       URL to test (default "http://localhost")
```

Run multiple http checks:
```
checker http multi [flags]

Flags:
  -e, --http-expected int     Expected HTTP response code (default 200)
  -u, --http-url string       URL to test (default "http://localhost")
  -e, --http-expected int     Expected HTTP response code (default 200)
  -u, --http-url string       URL to test (default "http://localhost")
```

Run a specific test command:
```
checker cmd [flags]

Flags:
  -c, --cmd-command string   Command to run
  -p, --cmd-pattern string   Pattern to match. Support regex.
  -s, --cmd-stop string      Command to shutdown (default "docker-compose down -v")
```