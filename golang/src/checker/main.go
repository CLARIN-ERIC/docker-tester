package main

import (
	"checker/commands"
)

func main() {
	commands.Execute()
}