package http

import (
	"bufio"
	"crypto/tls"
	"errors"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

type Check struct {
	Verbose     *bool
	Timeout     int
	TestTimeout *int
	Insecure    bool
	OutputFile  string
}

func (c *Check) SingleCheck(url string, expected int, headers []string) error {
	return performHttpCheck(c.Verbose, c.TestTimeout, url, expected, c.Insecure, c.Timeout, headers)
}

func (c *Check) MultiCheck(filename string) error {
	file, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		kv := strings.Split(line, ";")
		if len(kv) != 2 && len(kv) != 3 {
			return errors.New(fmt.Sprintf("Invalid line: %s", line))
		}
		url := kv[0]
		s_expected := kv[1]
		expected, err := strconv.Atoi(s_expected)
		if err != nil {
			return err
		}
		var headers []string
		if len(kv) == 3 {
			headers = strings.Split(kv[2], ",")
		}
		if *c.Verbose {
			fmt.Printf("Parsed check from file: %s\n", line)
		}

		err = performHttpCheck(c.Verbose, c.TestTimeout, url, expected, c.Insecure, c.Timeout, headers)
		if err != nil {
			return err
		}

	}

	if err := scanner.Err(); err != nil {
		return err
	}

	return nil
}

func performHttpCheck(verbose *bool, test_timeout *int, url string, expected int, insecure bool, http_timeout int, headers []string) error {
	threshold := time.Second * time.Duration(*test_timeout)

	if *verbose {
		fmt.Printf(
			"Running http test on %s, expecting http response code: %d, insecure: %t, http timeout: %ds, test timeout: %v\n",
			url, expected, insecure, http_timeout, threshold)
	}

	var err error
	start := time.Now()
	ticker := time.NewTicker(time.Second * time.Duration(1))
	for t := range ticker.C {
		err = issueHttpRequest(url, expected, insecure, http_timeout, verbose, headers)
		//Exit loop if test succeeded, i.e. no error was returned
		if err == nil {
			if *verbose {
				fmt.Printf("PASSED\n")
			}
			break
		} else {
			if *verbose {
				fmt.Printf("FAILED: %s\n", err)
			}
		}
		//Check for timeout, stop loop if timeout exceeded
		elapsed := time.Since(start)
		if elapsed > threshold {
			if false {
				fmt.Printf("%v", t)
			}
			err = errors.New(fmt.Sprintf("Timeout exceeded (%.2fs > %.2fs)", elapsed.Seconds(), threshold.Seconds()))
			break
		}
	}
	return err
}

func issueHttpRequest(url string, expected int, insecure bool, http_timeout int, verbose *bool, headers []string) error {
	//Initialize client
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: insecure},
	}
	httpTimeout := time.Duration(time.Duration(http_timeout) * time.Second)
	httpClient := http.Client{
		Timeout:   httpTimeout,
		Transport: tr,
	}

	//Build request
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return err
	}

	//Add headers
	for _, header := range headers {
		parts := strings.Split(header, ":")
		key := strings.Trim(parts[0], " ")
		value := strings.Trim(parts[1], " ")
		if *verbose {
			fmt.Printf("Adding header: %s: %s\n", key, value)
		}
		if key == "Host" {
			req.Host = value
		} else {
			req.Header.Set(key, value)
		}
	}

	//Execute request
	resp, err := httpClient.Do(req)
	if err != nil {
		return err
	}
	actual := resp.StatusCode

	//Evaluate http response code against expected result
	if actual == expected {
		return nil
	} else {
		return errors.New(fmt.Sprintf("Failed. HTTP response = %d, expected %d", actual, expected))
	}
}
