package test

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"
)

type Configuration struct {
	Verbose          bool
	OutputFile       string
	WaitForExtension string
	WaitSleepTime    time.Duration
	WaitTimeout      time.Duration
}

func NewDefaultTestConfiguration() *Configuration {
	return &Configuration{
		OutputFile: "/test/done", //File to write when all test are finished and all files with the
		// specified extenstions have disappeared.
		WaitTimeout:      10 * 60 * time.Second, //Total test runtime. If more time elapsed stop and fail test.
		WaitSleepTime:    1 * time.Second,       //Time to sleep between extension checks
		WaitForExtension: ".pid",                //Wait for all files with this extension to disappear before writing
		// the output file
	}
}

func NewTestConfiguration(verbose *bool, testTimeout, testWaitSleepTime *int, testSuccessFile, testWaitForExtension *string) *Configuration {
	testCfg := NewDefaultTestConfiguration()
	if verbose != nil {
		testCfg.Verbose = *verbose
	}
	if testSuccessFile != nil {
		testCfg.OutputFile = *testSuccessFile
	}
	if testTimeout != nil {
		testCfg.WaitTimeout = time.Duration(*testTimeout) * time.Second
	}
	if testWaitForExtension != nil {
		testCfg.WaitForExtension = *testWaitForExtension
	}
	if testWaitSleepTime != nil {
		testCfg.WaitSleepTime = time.Duration(*testWaitSleepTime) * time.Second
	}
	return testCfg
}

func writeTestEndFile(cfg *Configuration) {
	if _, err := os.OpenFile(cfg.OutputFile, os.O_RDONLY|os.O_CREATE, 0666); err != nil {
		fmt.Printf("Failed to write output file: %s\n", err.Error())
		os.Exit(1)
	} else {
		if cfg.Verbose {
			fmt.Printf("Output file written to: %s\n", cfg.OutputFile)
		}
		fmt.Printf("%s written\n", cfg.OutputFile)
	}
}

func FinishTest(cfg *Configuration, err error) {
	if err != nil {
		fmt.Printf("One or more tests FAILED: %s\n", err.Error())
		writeTestEndFile(cfg)
		os.Exit(1)
	}
	fmt.Printf("All tests PASSED\n")

	//wait with exit until no *.pid files exist anymore in parent dir
	/*
		if cfg.Verbose {
			jsonBytes, _ := json.MarshalIndent(cfg, "", "  ")
			fmt.Printf("Cfg: %s\n", string(jsonBytes))
		}
	*/
	parent := filepath.Dir(cfg.OutputFile)
	if err := waitForExit(parent, cfg.WaitForExtension, cfg.WaitSleepTime, cfg.WaitTimeout, cfg.Verbose); err != nil {
		fmt.Printf("waitForExit failed: %s\n", err.Error())
		writeTestEndFile(cfg)
		os.Exit(1)
	}

	writeTestEndFile(cfg)
}

func waitForExit(dir, ext string, sleep, totalWaitThreshhold time.Duration, verbose bool) error {
	tStart := time.Now()
	for {
		files, err := getFilenamesWithExtension(dir, ext)
		if err != nil {
			return fmt.Errorf("failed to get filenames with extenstion: %s", err)
		}

		//Run again if there is at least one file in the listing matching the supplied extension
		if len(files) <= 0 {
			break
		}

		//Check for exceeding the wait timeout
		totalWaitDuration := time.Since(tStart)
		if totalWaitDuration.Milliseconds() > totalWaitThreshhold.Milliseconds() {
			return fmt.Errorf("total test timeout exceeded")
		}

		//Sleep before next iteration
		if verbose {
			if len(files) == 1 {
				fmt.Printf("Waiting for 1 other test process to finish. 1 file in %s with the *%s extension\n", dir, ext)
			} else {
				fmt.Printf("Waiting for %d other test processes to finish. %d file in %s with the *%s extension\n", len(files), len(files), dir, ext)
			}
			//for _, f := range files {
			//	fmt.Printf("  %s\n", f)
			//}
		}
		time.Sleep(sleep)
	}

	fmt.Printf("All other processes have finished. No files in %s with the *%s extension.\n", dir, ext)
	return nil
}

func getFilenamesWithExtension(dir, ext string) ([]string, error) {
	var result []string

	err := filepath.Walk(fmt.Sprintf("%s/", dir), func(path string, info os.FileInfo, err error) error {
		if err == nil {
			if strings.HasSuffix(path, ext) {
				result = append(result, info.Name())
			}
		} else {
			fmt.Printf("%s\n", err)
		}
		return nil
	})

	if err != nil {
		return nil, err
	}

	return result, nil
}
