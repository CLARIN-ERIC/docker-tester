package commands

import (
	"checker/http"
	"checker/test"
	"github.com/spf13/cobra"
)

func InitHttpCommand(verbose *bool, testTimeout, testWaitSleepTime *int, testSuccessFile, testWaitForExtension *string) *cobra.Command {
	var url string
	var result int
	var insecure bool
	var httpTimeout int
	var file string

	var cmdHttpSingle = &cobra.Command{
		Use:   "single",
		Short: "Run single HTTP(s) check",
		Long:  `Run a single http check`,
		Run: func(cmd *cobra.Command, args []string) {
			testCfg := test.NewTestConfiguration(verbose, testTimeout, testWaitSleepTime, testSuccessFile, testWaitForExtension)
			check := http.Check{Verbose: verbose, Timeout: httpTimeout, TestTimeout: testTimeout, Insecure: insecure, OutputFile: *testSuccessFile}
			var headers []string
			test.FinishTest(testCfg, check.SingleCheck(url, result, headers))
		},
	}
	cmdHttpSingle.Flags().StringVarP(&url, "http-url", "u", "http://localhost", "URL to test")
	cmdHttpSingle.Flags().IntVarP(&result, "http-expected", "e", 200, "Expected HTTP response code")

	var cmdHttpMulti = &cobra.Command{
		Use:   "multi",
		Short: "Run multiple HTTP(s) checks",
		Long:  `Read multiple checks from file and run all of them`,
		Run: func(cmd *cobra.Command, args []string) {
			testCfg := test.NewTestConfiguration(verbose, testTimeout, testWaitSleepTime, testSuccessFile, testWaitForExtension)
			check := http.Check{Verbose: verbose, Timeout: httpTimeout, TestTimeout: testTimeout, Insecure: insecure, OutputFile: *testSuccessFile}
			test.FinishTest(testCfg, check.MultiCheck(file))
		},
	}
	cmdHttpMulti.Flags().StringVarP(&file, "http-input", "i", "/etc/checker.conf", "File specifying multiple checks")

	var cmdHttp = &cobra.Command{
		Use:   "http",
		Short: "HTTP(s) checking",
		Long:  `Check HTTP and HTTP endpoints`,
	}
	cmdHttp.PersistentFlags().BoolVarP(&insecure, "http-insecure", "k", false, "Allow connections to SSL sites without certs or with invalid certs")
	cmdHttp.PersistentFlags().IntVarP(&httpTimeout, "http-timeout", "t", 10, "Timeout in seconds")

	//Add subcommands
	cmdHttp.AddCommand(cmdHttpSingle)
	cmdHttp.AddCommand(cmdHttpMulti)

	/*
		var cmdHttp = &cobra.Command{
			Use:   "http",
			Short: "HTTP(s) checking",
			Long:  `Check HTTP and HTTP endpoints`,
			Run: func(cmd *cobra.Command, args []string) {
				testCfg := test.NewTestConfiguration(testTimeout, testWaitSleepTime, testSuccessFile, testWaitForExtension)
				err := http.PerformHttpCheck(verbose, testTimeout, url, result, insecure, httpTimeout)
				test.FinishTest(testCfg, err)
			},
		}

		cmdHttp.Flags().StringVarP(&url, "http-url", "u", "http://localhost", "URL to test")
		cmdHttp.Flags().IntVarP(&result, "http-expected", "e", 200, "Expected HTTP response code")
		cmdHttp.Flags().BoolVarP(&insecure, "http-insecure", "k", false, "Allow connections to SSL sites without certs or with invalid certs")
		cmdHttp.Flags().IntVarP(&httpTimeout, "http-timeout", "t", 10, "Http timeout in seconds")
	*/
	return cmdHttp
}

/*
func (c *Check) HandleResult(err error) (error) {
	if err != nil {
		fmt.Printf("Failed: %v. Writing file %s\n", err, c.output_file)
		os.OpenFile(c.output_file, os.O_RDONLY|os.O_CREATE, 0666)
		return err
	} else {
		fmt.Printf("Success, writing %s\n", c.output_file)
		os.OpenFile(c.output_file, os.O_RDONLY|os.O_CREATE, 0666)
		return nil
	}
}
*/
