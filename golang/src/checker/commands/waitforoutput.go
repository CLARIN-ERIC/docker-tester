package commands

import (
	"bufio"
	"checker/test"
	"context"
	"fmt"
	"github.com/spf13/cobra"
	"os/exec"
	"regexp"
	"strings"
	"time"
)

func InitWaitForOutputCommand(verbose *bool, testTimeout, testWaitSleepTime *int, testSuccessFile, testWaitForExtension *string) *cobra.Command {
	var command string
	var commandStop string
	var pattern string

	var cmd = &cobra.Command{
		Use:   "cmd",
		Short: "Wait for a specific pattern in the commands output",
		Long:  `Wait for a specific pattern in the commands output`,
		Run: func(cmd *cobra.Command, args []string) {
			testCfg := test.NewTestConfiguration(verbose, testTimeout, testWaitSleepTime, testSuccessFile, testWaitForExtension)
			err := waiForOutput(command, commandStop, pattern, testCfg.WaitTimeout)
			test.FinishTest(testCfg, err)
		},
	}
	cmd.Flags().StringVarP(&pattern, "cmd-pattern", "p", "", "Pattern to match. Support regex.")
	cmd.Flags().StringVarP(&command, "cmd-command", "c", "", "Command to run")
	cmd.Flags().StringVarP(&commandStop, "cmd-stop", "s", "docker-compose down -v", "Command to shutdown")

	return cmd
}

/**
Run the supplied command until the pattern is found in the output or until the timeout is exceeded.
*/
func waiForOutput(command, commandStop, pattern string, testTimeout time.Duration) error {
	rpat := regexp.MustCompile(fmt.Sprintf("^.*%s.*$", pattern))
	ctx, cancel := context.WithTimeout(context.Background(), testTimeout)
	defer cancel()

	args := strings.Fields(command)
	cmd := exec.CommandContext(ctx, args[0], args[1:]...)

	stdout, _ := cmd.StdoutPipe()
	stderr, _ := cmd.StderrPipe()

	if err := cmd.Start(); err != nil {
		return fmt.Errorf("Failed. Reason: failed to start command, error: %s.\n", err)
	}

	go func() {
		// Wait until timeout or deferred cancellation
		<-ctx.Done()
		//cmd.Process.Signal(syscall.SIGTERM)

		args = strings.Fields(commandStop)
		if err := exec.Command(args[0], args[1:]...).Run(); err != nil {
			fmt.Printf("Stop command failed: %s\n", err)
		}
	}()

	go func() {
		scanner1 := bufio.NewScanner(stdout)
		for scanner1.Scan() {
			m := scanner1.Text()
			fmt.Printf("%s\n", m)
			if rpat.MatchString(m) {
				fmt.Printf("Matched pattern: [%s]\n", pattern)
				args = strings.Fields(commandStop)
				if err := exec.Command(args[0], args[1:]...).Run(); err != nil {
					fmt.Printf("Stop command failed: %s\n", err)
				}
			}
		}
	}()

	go func() {
		scanner2 := bufio.NewScanner(stderr)
		for scanner2.Scan() {
			m := scanner2.Text()
			fmt.Printf("%s\n", m)
			if rpat.MatchString(m) {
				fmt.Printf("Matched pattern: [%s]\n", pattern)
				args = strings.Fields(commandStop)
				if err := exec.Command(args[0], args[1:]...).Run(); err != nil {
					fmt.Printf("Stop command failed: %s\n", err)
				}
			}
		}
	}()

	cmd.Wait()

	if ctx.Err() != nil {
		return fmt.Errorf("FAILED. Reason: pattern not found, timeout (%.2f seconds) exceeded.\n", testTimeout.Seconds())
	}

	return nil
}
