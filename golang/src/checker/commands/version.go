package commands

import (
	"fmt"
	"github.com/spf13/cobra"
)

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version number of checker",
	Long:  `All software has versions. This is checker's.`,
	Run: func(cmd *cobra.Command, args []string) {
		printCheckerVersion()
	},
}

func printCheckerVersion() {
	fmt.Printf("Checker v%s by CLARIN ERIC\n", "2.0.0")
}
