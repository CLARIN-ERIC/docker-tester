package commands

import (
	"github.com/spf13/cobra"
)

var CheckerCmd = &cobra.Command{
	Use:   "checker",
	Short: "Perform automated tests on a service",
	Long:  `Perform automated tests on a service.`,
}

func Execute() {

	const (
		defaultTestTimeout          = 60
		defaultTestWaitSleepTime    = 1
		defaultTestSuccessFile      = "/test/done"
		defaultTestWaitForExtension = ".pid"
	)

	var verbose bool
	var testTimeout int
	var testWaitSleepTime int
	var testSuccessFile string
	var testWaitForExtension string

	CheckerCmd.PersistentFlags().BoolVarP(&verbose, "verbose", "v", false, "Run in verbose mode")
	CheckerCmd.PersistentFlags().IntVar(&testTimeout, "timeout", defaultTestTimeout, "Total test timeout in seconds")
	CheckerCmd.PersistentFlags().StringVar(&testSuccessFile, "success-file", defaultTestSuccessFile, "Path to file to signal other container that test succeeded")
	CheckerCmd.PersistentFlags().StringVar(&testWaitForExtension, "wait-for-ext", defaultTestWaitForExtension, "Wait until all files with this extension are removed")
	CheckerCmd.PersistentFlags().IntVar(&testWaitSleepTime, "sleep", defaultTestWaitSleepTime, "Wait this amount of seconds before checking for extensions again")

	CheckerCmd.AddCommand(
		versionCmd,
		InitHttpCommand(&verbose, &testTimeout, &testWaitSleepTime, &testSuccessFile, &testWaitForExtension),
		InitWaitForOutputCommand(&verbose, &testTimeout, &testWaitSleepTime, &testSuccessFile, &testWaitForExtension),
	)
	CheckerCmd.Execute()
}
